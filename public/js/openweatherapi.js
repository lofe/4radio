class GermanSpeechSynthesisUtterance extends SpeechSynthesisUtterance {

  constructor(text) {
    super(text);
    this.lang = "de-DE";
  }

  speak() {
    window.speechSynthesis.speak(this);
    delete this;
  }

}


class Sky extends GermanSpeechSynthesisUtterance {

  constructor(value) {
    const text = "Das Wetter: " + value;
    super(text);
  }
  
}


class Temperature extends GermanSpeechSynthesisUtterance {

  constructor(value) {
    let text = value.toFixed(1).toString();
    text = text.replace(".1", ".ein").replace(".", " komma ");
    text += "Grad Celsius";
    text = "Die Temperatur beträgt " + text;
    super(text);
  }

}
 

class Time extends GermanSpeechSynthesisUtterance {

  constructor(value) {
    const hours = value.getHours().toString().replace("01", "ein");
    const minutes = value.getMinutes().toString().replace("00", "");
    const text = "Es ist " + hours + " Uhr " + minutes;
    super(text);
  }

}


class Weather {

  constructor(id, dates=null) {
    this.dates = dates;
    this.id = id;
    this.sky = null;
    this.temperature = null;
    this.time = null;
    this.getForecast = this.getForecast.bind(this);
    this.broadcast = this.broadcast.bind(this);
    this.speak = this.speak.bind(this);
    this.getForecast();
    setInterval(this.getForecast, 600000);
    if (this.dates) {
      this.broadcast(false);
    }
  }

  getForecast() {
    const self = this;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE) {
        if (xmlhttp.status == 200) {
          const response = JSON.parse(xmlhttp.responseText);
          const now = response.list[0];
          self.sky = now.weather[0].description;
          self.temperature = now.main.temp;
        }
        else {
          console.log(xmlhttp.status);
        }
      }
    };
    let url = "https://api.openweathermap.org/data/2.5/forecast";
    url += "?id=" + this.id;
    url += "&appid=" + "d474b42560ae861a5c895e34ead1f4d1";
    url += "&lang=de";
    url += "&units=metric";
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

  broadcast(speak=true) {
    if (speak) {
      const audio = new Audio('ogg/intro.ogg');
      audio.onended = this.speak;
      audio.play();
    }
    const now = new Date();
    let timeout = 86400000;
    for (var index = 0; index < this.dates.length; ++index) {
      let difference = this.dates[index] - now;
      if (difference < 0) {
        difference += 86400000;
      }
      if (difference < timeout) {
        timeout = difference;
      }
    }
    setTimeout(this.broadcast, timeout);
  }

  speak() {
    if (!this.temperature) {
      return;
    }
    const now = new Date();
    const time = new Time(now);
    time.speak();
    const temperature = new Temperature(this.temperature);
    temperature.speak();
    const sky = new Sky(this.sky);
    sky.speak();
  }

}
